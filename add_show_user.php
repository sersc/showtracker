<?php
session_start();
include("config/config.php");
include("include/funciones.php");

$id_show = $_REQUEST["id_show"];
$add = $_REQUEST["add"];

// Connect to database.
$db = new Db();

if($add == "true"){
    $sql = "INSERT INTO `user-show` (id_show, id_user) VALUES(?, ?)";
}
else{
    $sql = "DELETE FROM `user-show` WHERE id_show = ? AND id_user = ?";
}

$result = $db->launchQuery($sql, array($id_show, $_SESSION["id"]));
$db->disconnect();

header("Location: index.php?id=shows&id_shows=".$id_show);