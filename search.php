<?php
// Shows that will show when user uses the search box.
$db = new Db();

$type = $_REQUEST["type"]; // TABLE_NAME
$search = $_REQUEST["search"];
if(!isset($_REQUEST["order"])) {
    $sql = "SELECT * FROM " . $type . " WHERE name like '{$search}%'";
}
// From sidebar search
else if(isset($_REQUEST["field"])){
    $order = $_REQUEST["order"];
    $field = $_REQUEST["field"];
    $sql = "SELECT * FROM " . $type . " WHERE " . $field . " like '{$search}%' ORDER BY " . $order . " ASC";
}
else if(isset($_REQUEST["order"])){
    $order = $_REQUEST["order"];
    $sql = "SELECT * FROM " . $type . " WHERE name like '{$search}%' ORDER BY " . $order . " ASC";
}

$result = $db->launchQuery($sql);
$db->disconnect();

?>

<!-- Order box -->
<div class="sidebar-module sidebar-module-inset" style="width: 350px; margin-bottom: 2rem;">
    <h4>Order By</h4>
    <form action="index.php" method="post" class="form-inline mt-2 mt-md-0">
        <select name="order" class="custom-select" style="margin-right: 0.5rem;">
            <?php
            echo $type;
            if ($type == "shows"){
                ?>
                <option value='name' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'name'){ echo 'selected';} ?>>Name</option>";
                <option value='duration' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'duration'){ echo 'selected';} ?>>Duration</option>";
                <option value='seasons' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'seasons'){ echo 'selected';} ?>>Seasons</option>";
                <?php
            }
            else if($type == "cast"){
                ?>
                <option value='name' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'name'){ echo 'selected';} ?>>Name</option>";
                <?php
            }
            else if($type == "network"){
                ?>
                <option value='name' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'name'){ echo 'selected';} ?>>Name</option>";
                <option value='country' <?php if(isset($_REQUEST['order']) && $_REQUEST['order'] == 'country'){ echo 'selected';} ?>>Country</option>";
                <?php
            }
            ?>
        </select>
        <input type="hidden" name="id" value="search"/>
        <input type="hidden" name="search" value="<?=$search?>"/>
        <input type="hidden" name="type" value="<?=$type?>"/>

        <?php if(isset($_REQUEST["field"])){ ?>
            <input type="hidden" name="field" value="<?=$field?>"/>
        <?php
        }
        ?>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Order</button>
    </form>
</div>

<!-- Search results -->
<div class="row">
    <?php
    while ( $row = $result->fetch_assoc()) {
        ?>
        <div class="card" style="width: 12rem; margin: 0.5rem 0.5rem;">
            <a href="?id=<?=$type?>&id_<?=$type?>=<?= $row["id"]?>">
                <img class="card-img-top" src="img/<?= $row["photo"] ?>">
            </a>
            <div class="card-body">
                <a href="?id=<?=$type?>&id_<?=$type?>=<?= $row["id"]?>" class="card-title" style="font-size: 20px; font-weight: bold; font-family: Helvetica;">
                    <?= $row["name"] ?>
                </a>
            </div>
        </div>
        <?php
    }
    ?>
</div>