<?php
if (isset($_REQUEST["message"]))
    $message = $_REQUEST["message"];
else
    $message = "";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <form class="form-signin" action="check_login.php" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>

        <label for="user" class="sr-only">User</label>
        <input type="text" id="user" name="user" class="form-control" placeholder="Username" required autofocus>

        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>

        <a href="register.php"> > Sign up here</a>
        <button class="btn btn-lg btn-primary btn-block form-button" type="submit">Log in</button>
    </form>

    <?php
    if ($message != "") {
        ?>
        <div class="alert alert-danger" role="alert"><p> <?= $message ?> </p></div>
        <?php
    }
    ?>

</div> <!-- /container -->
</body>
</html>
