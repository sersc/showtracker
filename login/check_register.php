<?php
include("../config/config.php");
include("../include/funciones.php");

$db = new Db();

$name = $_POST["name"];
$user = $_POST["user"];
$password = $_POST["password"];

$sql = "INSERT INTO users SET name = ?, username = ?, password = SHA1(?), rol = ?, joined = ?";
$resultado = $db->launchQuery($sql, array($name, $user, $password, "User", date("Y-m-d H:i:s")));
$lastid = $db->lastID();
$db->disconnect();

session_start();
$_SESSION["user"] = $user;
$_SESSION["id"] = $lastid;
header('Location: ../index.php');