<?php
include("../config/config.php");
include("../include/funciones.php");

$db = new Db();

$user = $_POST["user"];
$password = $_POST["password"];

$sql = "SELECT id, rol FROM users WHERE username = ? AND password = SHA1(?)";
$result = $db->launchQuery($sql, array($user, $password));
$row = $result->fetch_assoc();

$db->disconnect();

if ($result->num_rows == 0){
    // No coincide
    header('Location: login.php?message=Error al introducir usuario y contraseña');
    exit();
}
// Si coinciden
session_start();
$_SESSION["user"] = $user;
$_SESSION["id"] = $row["id"];
$_SESSION["rol"] = $row["rol"];
header('Location: ../index.php');