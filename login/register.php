<?php
if (isset($_REQUEST["message"]))
    $message = $_REQUEST["message"];
else
    $message = "";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <form class="form-signin" action="check_register.php" method="post">
        <h2 class="form-signin-heading">Please sign up</h2>

        <label for="name" class="sr-only">Name</label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>

        <label for="user" class="sr-only">User</label>
        <input type="text" id="user" name="user" class="form-control" placeholder="Username" required autofocus>

        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block form-button" type="submit">Sign up</button>
    </form>

    <?php
    if ($message != "") {
        ?>
        <div class="alert alert-danger" role="alert"><p> <?= $message ?> </p></div>
        <?php
    }
    ?>

</div> <!-- /container -->
</body>
</html>
