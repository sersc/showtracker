 <?php
class Db {

    private $conn;

    function __construct() {
        $this->connect();
    }

    function connect() {
        $this->conn = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);
    }

    function disconnect () {
        $this->conn->close();
    }

    function lastID () {
        return $this->conn->insert_id;
    }

    function launchQuery($sql, $parametros = array()) {

        $sentencia = $this->conn->prepare($sql);

        if (!empty($parametros)){
            $tipos = "";
            foreach ($parametros as $param) {
                if (gettype($param) == "string")
                    $tipos = $tipos . "s";
                else
                    $tipos = $tipos . "i";
            }
            $sentencia->bind_param($tipos, ...$parametros);
        }

        $sentencia->execute();
        $resultado = $sentencia->get_result();
        $sentencia->close();

        return $resultado;
    }
}


