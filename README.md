# ShowTracker #

This project allows you to create users and store shows on a list. It also gives you information
about the details of the show, who acts in that show and which network produced the show.

### Getting Started ###

* Go to [ShowTracker](http://217.182.169.85/~ssoler/)
* Create an user
* Navigate though the home page and search for shows to add them to your profile.

### Libraries ###

* CKEditor

    Download CKEditor 'Downloads' and add it to a root folder called 'libs'

### Built With ###

* PHP
* Bootstrap

### Authors ###

* Sergio Soler