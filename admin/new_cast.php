<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
include("../config/config.php");
include("../include/funciones.php");

// UPDATE VARIABLES
if(isset($_REQUEST["update"])){
    $update = $_REQUEST["update"];
    $id = $_REQUEST["id"];
}

$name = $_POST["name"];
$biography = $_POST["biography"];
$birthday = $_POST["birthday"];
$role = $_POST["role"];
$photo = $_FILES["photo"]["name"];
$tmp_photo = $_FILES["photo"]["tmp_name"];

$photo_path = "../img/" . $photo;
move_uploaded_file($tmp_photo, $photo_path);

// Connect to database.
$db = new Db();

if($update){
    $sql = "UPDATE cast SET name = ?, biography = ?, birthday = ?, role = ?, photo = ? WHERE id = " . $id;
}
else{
    $sql = "INSERT INTO cast (name, biography, birthday, role, photo) VALUES (?, ?, ?, ?, ?)";
}

$db->launchQuery($sql, array($name, $biography, $birthday, $role, $photo));
$db->disconnect();

header("Location: index.php");
