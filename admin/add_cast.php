<?php
if (!isset($_SESSION["user"])) {
    header("Location: ../login/login.php");
}
?>
<div class="container">
    <form action="add_new_cast.php" method="post">
        <div class="form-row">
            <?php
            $db = new Db();
            $result = $db->launchQuery("SELECT id, name FROM shows");
            ?>
            <!-- Show select -->
            <div class='form-group col-md-5'>
                <select name="show" style='width: 350px;' size="10">
                <?php
                while($row = $result->fetch_assoc()) {
                    ?>
                    <option value="<?=$row["id"]?>"><?=$row["name"]?></option>
                    <?php
                }
                ?>
                </select>
            </div>

            <!-- Cast multiple select -->
            <div class='form-group col-md-5'>
                <select name="cast[]" style='width: 350px;' multiple size="10">
                <?php
                $result = $db->launchQuery("SELECT id, name FROM cast");
                while($row = $result->fetch_assoc()) {
                    ?>
                    <option value="<?=$row["id"]?>"><?=$row["name"]?></option>
                    <?php
                }
                ?>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 15px;">Add Cast</button>
    </form>
</div>