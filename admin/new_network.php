<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
include("../config/config.php");
include("../include/funciones.php");

// UPDATE VARIABLES
if(isset($_REQUEST["update"])){
    $update = $_REQUEST["update"];
    $id = $_REQUEST["id"];
}

$name = $_POST["name"];
$country = $_POST["country"];
$founded = $_POST["founded"];
$website = $_POST["website"];
$language = $_POST["language"];
$photo = $_FILES["photo"]["name"];
$tmp_photo = $_FILES["photo"]["tmp_name"];

$photo_path = "../img/" . $photo;
move_uploaded_file($tmp_photo, $photo_path);

// Connect to database.
$db = new Db();

if($update){
    $sql = "UPDATE network SET name = ?, country = ?, founded = ?, website = ?, language = ?, photo = ? WHERE id = " . $id;
}
else{
    $sql = "INSERT INTO network (name, country, founded, website, language, photo) VALUES (?, ?, ?, ?, ?, ?)";
}

$db->launchQuery($sql, array($name, $country, $founded, $website, $language, $photo));
$db->disconnect();

header("Location: index.php");
