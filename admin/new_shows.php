<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
include("../config/config.php");
include("../include/funciones.php");

// UPDATE VARIABLES
if(isset($_REQUEST["update"])){
    $update = $_REQUEST["update"];
    $id = $_REQUEST["id"];
}

$name = $_POST["name"];
$seasons = $_POST["seasons"];
$synopsis = $_POST["synopsis"];
$genre = $_POST["genre"];
$photo = $_FILES["photo"]["name"];
$tmp_photo = $_FILES["photo"]["tmp_name"];
$year = $_POST["year"];
$duration = $_POST["duration"];
$network = $_POST["network"];
if (isset($_POST["finished"]))
    $finished = 1;
else
    $finished = 0;
$trailer = $_POST["trailer"];

$photo_path = "../img/" . $photo;
move_uploaded_file($tmp_photo, $photo_path);

// Connect to database.
$db = new Db();

if($update){
    // UPDATE SHOW
    $sql = "UPDATE shows SET name = ?, seasons = ?, synopsis = ?, genre = ?, photo = ?, year = ?,
            duration = ?, network_id = ?, finished = ?, trailer = ? WHERE id = " . $id;
    $db->launchQuery($sql, array($name, $seasons, $synopsis, $genre, $photo, $year, $duration, $network, $finished, $trailer));

    // UPDATE RELATION BETWEEN SHOW AND NETWORK.
    $sql = "UPDATE `network-show` SET id_network = ? WHERE id_show = ?";
    $db->launchQuery($sql, array($network, $id));
}
else{
    // INSERT SHOW TO DATABASE
    $sql = "INSERT INTO shows (name, seasons, synopsis, genre, photo, year, duration, network_id, finished, trailer) 
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $db->launchQuery($sql, array($name, $seasons, $synopsis, $genre, $photo, $year, $duration, $network, $finished, $trailer));

// INSERT RELATION BETWEEN SHOW AND NETWORK.
    $sql = "INSERT INTO `network-show` (id_network, id_show) VALUES (?, ?)";
    $db->launchQuery($sql, array($network, $db->lastID()));
}

$db->disconnect();

header("Location: index.php");
