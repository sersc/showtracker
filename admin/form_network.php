<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin' ) {
    header("Location: ../login/login.php");
}

if (isset($_REQUEST["id_item"])){
    $update = true;
    $db = new Db();
    $sql = "SELECT * FROM network WHERE id = " . $_REQUEST['id_item'];
    $result = $db->launchQuery($sql);
    $show = $result->fetch_assoc();
}
else
    $update = false;
?>

<div class="container">
    <form action="new_network.php<?php if($update) echo "?update=true&id=" . $_REQUEST["id_item"]?>" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       value="<?php if($update) echo $show["name"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="country">Country</label>
                <input type="text" class="form-control" id="country" name="country" placeholder="Country"
                       value="<?php if($update) echo $show["country"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="founded">Founded</label>
                <input type="number" class="form-control" id="founded" name="founded" placeholder="Founded (Year)"
                       value="<?php if($update) echo $show["founded"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="language">Language</label>
                <input type="text" class="form-control" id="language" name="language" placeholder="Language"
                       value="<?php if($update) echo $show["language"]?>">
            </div>
        </div>

        <div class="form-row" style="margin-top: 0.5rem;">
            <div class="form-group col-md-6">
                <label for="photo">Photo</label>
                <input type="file" class="form-control" id="photo" name="photo" placeholder="Photo">
            </div>
            <div class="form-group col-md-6">
                <label for="website">Website</label>
                <input type="text" class="form-control" id="website" name="website" placeholder="Website"
                       value="<?php if($update) echo $show["website"]?>">
            </div>

        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 15px;"><?php if($update) echo "Update "; else echo "Create ";?> Network</button>
    </form>
</div>