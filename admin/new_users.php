<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
include("../config/config.php");
include("../include/funciones.php");

// UPDATE VARIABLES
if(isset($_REQUEST["update"])){
    $update = $_REQUEST["update"];
    $id = $_REQUEST["id"];
}

$name = $_POST["name"];
$username = $_POST["username"];
$rol = $_POST["rol"];

// Connect to database.
$db = new Db();
if($update){
    $sql = "UPDATE users SET name = ?, username = ?, rol = ? WHERE id = " . $id;
}
else{
    $sql = "INSERT INTO users (name, username, rol) VALUES (?, ?, ?)";
}

$db->launchQuery($sql, array($name, $username, $rol));
$db->disconnect();

header("Location: index.php");
