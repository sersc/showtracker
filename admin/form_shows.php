<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}

if (isset($_REQUEST["id_item"])){
    $update = true;
    $db = new Db();
    $sql = "SELECT * FROM shows WHERE id = " . $_REQUEST['id_item'];
    $result = $db->launchQuery($sql);
    $show = $result->fetch_assoc();
}
else
    $update = false;
?>
<div class="container">
    <form action="new_shows.php<?php if($update) echo "?update=true&id=" . $_REQUEST["id_item"]?>" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name"
                       placeholder="Name" value="<?php if($update) echo $show["name"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="genre">Genre</label>
                <input type="text" class="form-control" id="genre" name="genre"
                       placeholder="Genre" value="<?php if($update) echo $show["genre"]?>">
            </div>
            <div class="form-group col-md-2">
                <label for="seasons">Seasons</label>
                <input type="number" class="form-control" id="seasons" name="seasons"
                       placeholder="Seasons" value="<?php if($update) echo $show["seasons"]?>">
            </div>
            <div class="form-group col-md-2">
                <label for="duration">Duration</label>
                <input type="number" class="form-control" id="duration" name="duration"
                       placeholder="Duration (min)" value="<?php if($update) echo $show["duration"]?>">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="synopsis">Synopsis</label>
                <textarea type="text" class="form-control" id="synopsis" name="synopsis" rows="10" cols="80"
                          placeholder="Synopsis"><?php if($update) echo $show["synopsis"]?></textarea>
            </div>
        </div>

        <div class="form-row" style="margin-top: 0.5rem;">
            <div class="form-group col-md-6">
                <label for="photo">Photo</label>
                <input type="file" class="form-control" id="photo" name="photo" placeholder="Photo">
            </div>
            <div class="form-group col-md-3">
                <label for="year">Year</label>
                <input type="number" class="form-control" id="year" name="year"
                       placeholder="Year (Premiere)" value="<?php if($update) echo $show["year"]?>">
            </div>

            <div class="form-group col-md-3">
                <p>Network</p>
                <select name="network">
                    <?php
                    $db = new Db();
                    $result = $db->launchQuery("SELECT id, name FROM network");
                    while($row = $result->fetch_assoc()) {
                        ?>
                        <option value="<?= $row["id"]?>" <?php if($update && $row["id"] == $show["network_id"]) echo "selected"; ?>>
                            <?= $row["name"]?></option>
                        <?php
                    }
                    $db->disconnect();
                    ?>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="trailer">Trailer</label>
                <input type="text" class="form-control" id="trailer" name="trailer"
                       placeholder="Youtube trailer" value="<?php if($update) echo $show["trailer"]?>">
            </div>
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="finished" name="finished"
                       value="Finished" <?php if($update && $show["finished"]==1) echo "checked"; ?>> Finished
            </label>
        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 15px;"><?php if($update) echo "Update "; else echo "Create ";?> Show</button>
    </form>
</div>
<script>
    CKEDITOR.replace("synopsis");
</script>