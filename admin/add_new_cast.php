<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
include("../config/config.php");
include("../include/funciones.php");

$show = $_POST["show"];
$cast = $_POST["cast"];

// Connect to database.
$db = new Db();

foreach ($cast as $person) {
    // Check if the person is already in the show.
    $sql = "SELECT * FROM `cast-show` WHERE id_show = " . $show . " AND id_cast = " . $person;
    $result = $db->launchQuery($sql);
    // Do not exist
    if ($result->num_rows == 0) {
        $sql = "INSERT INTO `cast-show` (id_show, id_cast) VALUES (?, ?)";
        $db->launchQuery($sql, array($show, $person));
    }
}

$db->disconnect();

header("Location: index.php");