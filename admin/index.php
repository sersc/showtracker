<?php
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin')
    header("Location: ../login/login.php");

// Table id
if(!isset($_REQUEST["id"]))
    $id = "shows";
else
    $id = $_REQUEST["id"];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">
    <!-- CK Editor -->
    <script src="../libs/ckeditor/ckeditor.js"></script>
</head>

<body>
<header>
    <div class="blog-masthead">
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="../index.php">ShowTracker</a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="../index.php">Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>

<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">

            <!-- Overview -->
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "overview") echo 'active' ?>" href="?id=overview">Overview</a>
                </li>
            </ul>

            <!-- Shows / Cast / Tv Network -->
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "shows") echo 'active' ?>" href="?id=shows">Shows</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "cast") echo 'active' ?>" href="?id=cast">Cast</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "network") echo 'active' ?>" href="?id=network">TV Network</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "users") echo 'active' ?>" href="?id=users">Users</a>
                </li>
            </ul>

            <!-- Add Cast to Shows / Add Shows to Network -->
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link <?php if($id == "add_cast") echo 'active' ?>" href="?id=add_cast">Add Cast to Show</a>
                </li>
            </ul>
        </nav>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
            <?php
            include("../config/config.php");
            include("../include/funciones.php");
            // If the id matches one of the names, go to the php file. Otherwise go to the table.
            if( preg_match('/(^form_|^add_|^delete_|^modify_|overview)/', $id) == 1){
                include($id . ".php");
            }
            else
                // Show results in a table according to its id.
                include("./table.php");
            ?>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script>
</body>
</html>




