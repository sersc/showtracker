<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin' ) {
    header("Location: ../login/login.php");
}

if (isset($_REQUEST["id_item"])){
    $update = true;
    $db = new Db();
    $sql = "SELECT * FROM cast WHERE id = " . $_REQUEST['id_item'];
    $result = $db->launchQuery($sql);
    $show = $result->fetch_assoc();
}
else
    $update = false;
?>
<div class="container">
    <form action="new_cast.php<?php if($update) echo "?update=true&id=" . $_REQUEST["id_item"]?>" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       value="<?php if($update) echo $show["name"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="birthday">Birthday</label>
                <input type="number" class="form-control" id="birthday" name="birthday" placeholder="Birthday (Year)"
                       value="<?php if($update) echo $show["birthday"]?>">
            </div>
            <div class="form-group col-md-3">
                <label for="role">Role</label>
                <input type="text" class="form-control" id="role" name="role" placeholder="Role"
                       value="<?php if($update) echo $show["role"]?>">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="biography">Biography</label>
                <textarea type="text" class="form-control" id="biography" name="biography" rows="10" placeholder="Biography">
                    <?php if($update) echo $show["biography"]?>
                </textarea>
            </div>
        </div>

        <div class="form-row" style="margin-top: 0.5rem;">
            <div class="form-group col-md-6">
                <label for="photo">Photo</label>
                <input type="file" class="form-control" id="photo" name="photo" placeholder="Photo">
            </div>

        </div>

        <button type="submit" class="btn btn-primary" style="margin-top: 15px;"><?php if($update) echo "Update "; else echo "Create ";?> Cast</button>
    </form>
</div>
<script>
    CKEDITOR.replace("biography");
</script>