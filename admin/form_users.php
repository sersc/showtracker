<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin' ) {
    header("Location: ../login/login.php");
}

if (isset($_REQUEST["id_item"])){
    $update = true;
    $db = new Db();
    $sql = "SELECT * FROM users WHERE id = " . $_REQUEST['id_item'];
    $result = $db->launchQuery($sql);
    $user = $result->fetch_assoc();
}
else
    $update = false;
?>

<div class="container">
    <form action="new_users.php<?php if($update) echo "?update=true&id=" . $_REQUEST["id_item"]?>" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       value="<?php if($update) echo $user["name"]?>">
            </div>
            <div class="form-group col-md-4">
                <label for="username">Username</label>
                <input type="username" class="form-control" id="username" name="username" placeholder="Username"
                       value="<?php if($update) echo $user["username"]?>">
            </div>
            <div class="form-group col-md-4">
                <label for="rol">Rol</label>
                <input type="text" class="form-control" id="rol" name="rol" placeholder="Rol"
                       value="<?php if($update) echo $user["rol"]?>">
            </div>
        </div>

        <?php if ($update){ ?>
            <button type="submit" class="btn btn-primary" style="margin-top: 15px;">Update User</button>
            <?php
        }
        ?>
    </form>
</div>