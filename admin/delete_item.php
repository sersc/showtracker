<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}

$db = new Db();
// type => table
$type = strtolower($_REQUEST["id_type"]);
$id_item = $_REQUEST["id_item"];

$sql = "DELETE FROM " . $type . " WHERE id = " . $id_item;
$resultado = $db->launchQuery($sql);

$db->disconnect();
header("Location: index.php");