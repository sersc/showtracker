<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}

$db = new Db();
?>
<h1>Overview</h1>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#Shows</th>
                <th>#Cast</th>
                <th>#Network</th>
                <th>#Users</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php
                $sql = "SELECT COUNT(*) FROM shows";
                $result = $db->launchQuery($sql);
                $row = $result->fetch_row();
                ?>
                    <td><?=$row[0]?></td>
                <?php
                $sql = "SELECT COUNT(*) FROM cast";
                $result = $db->launchQuery($sql);
                $row = $result->fetch_row();
                ?>
                    <td><?=$row[0]?></td>
                <?php
                $sql = "SELECT COUNT(*) FROM network";
                $result = $db->launchQuery($sql);
                $row = $result->fetch_row();
                ?>
                    <td><?=$row[0]?></td>
                <?php
                $sql = "SELECT COUNT(*) FROM users";
                $result = $db->launchQuery($sql);
                $row = $result->fetch_row();
                $db->disconnect()
                ?>
                    <td><?=$row[0]?></td>
            </tr>
        </tbody>
    </table>
</div>