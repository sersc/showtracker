<?php
if (!isset($_SESSION["user"]) || $_SESSION["rol"] != 'admin') {
    header("Location: ../login/login.php");
}
if(isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];

$columns = array();
?>

<h1><?= ucfirst($id) ?></h1>

<?php if ($id != "users") { ?>
    <a class="btn btn-primary" style="margin-bottom: 1rem;" href="?id=form_<?= $id ?>">Create <?= $id ?></a>
    <?php
    }
?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <?php
                $db = new Db();

                // Get how many fields are in this table
                $sql = "SELECT COLUMN_NAME 
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = '". $id . "'";

                $result = $db->launchQuery($sql);

                while($row = $result->fetch_row()){
                    array_push($columns, $row[0]);
                    ?>
                    <th><?= $row[0]; ?></th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>

            <?php

            // Get the results for each row
            $sql = "SELECT * FROM `" . $id . "`";
            $result = $db->launchQuery($sql);
            while($row = $result->fetch_row()){
                echo "<tr>";
                for ($i = 0; $i < count($columns); $i++) {
                    if ($id == "Shows" && $i == 3){
                        echo "<td>" . substr($row[$i], 0, 70) ."...</td>";
                    }
                    else{
                        echo "<td> $row[$i] </td>";
                    }
                }
                ?>
                <td><a class="btn btn-warning" href="?id=form_<?=$id?>&id_item=<?=$row[0]?>">Modify</a></td>
                <td><a class="btn btn-danger" href="?id=delete_item&id_type=<?=$id?>&id_item=<?=$row[0]?>">Delete</a></td>
                </tr>
                <?php
            }
            $db->disconnect();
            ?>
        </tbody>
    </table>
</div>
