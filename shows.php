<?php
// Shows site, with all the information
$id_shows = $_REQUEST["id_shows"];

// Connect
$db = new Db();

$result = $db->launchQuery("SELECT shows.*, network.name as network, network.id as netid FROM shows INNER JOIN network 
                              ON shows.network_id = network.id
                              WHERE shows.id = ?", array($id_shows));
$show = $result->fetch_assoc();

?>
<div class="row">
    <!-- Show details -->
    <div>
        <div class="card-body" style="width: 70%; float: right;">
            <h1 class="card-title"><?= $show["name"] ?></h1>
            <p class="card-title"><?= "Seasons: " . $show["seasons"] . " | " . $show["duration"] . " min | " .
                                        $show["genre"] . " | " . $show["year"]; ?></p>
            <div>
                <a target="_blank" href="?id=network&id_network=<?= $show["netid"]?>"><?= $show["network"]?></a>

                <!-- Add or remove shows from user -->
                <?php
                    if(isset($_SESSION["id"])) {
                        $sql = "SELECT COUNT(*) FROM `user-show` WHERE id_user = " . $_SESSION["id"] . " AND id_show = " . $id_shows;
                        $result = $db->launchQuery($sql);
                        $exist = $result->fetch_row();
                        ?>
                        <span style="margin-left: 20px;">
                            <img src="img/<?php if ($exist[0] == 1) echo "minus"; else echo "plus" ?>.jpg" width="50px">
                            <a href="add_show_user.php?id_show=<?=$show["id"]?>&add=<?php if($exist[0] == 1) echo "false"; else echo "true"?>"
                                style="color: <?php if ($exist[0] == 1) echo "red"; else echo "limegreen" ?>">
                                <?php if ($exist[0] == 1) echo "Remove"; else echo "Add" ?> show
                            </a>
                        </span>
                        <?php
                    }
                ?>

            </div>
            <p class="card-text" style="margin-top: 5px;"><?= $show["synopsis"] ?></p>
        </div>
        <img class="card-img-top" src="img/<?= $show["photo"] ?>" alt="Card image cap" style="width: 25%;">
    </div>

    <!-- Trailer -->
    <div style="width: 100%;">
        <hr>
        <h1 class="title-border">Trailer</h1>
        <div class="row">
            <iframe width="720" height="480" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen
                    src="<?= $show["trailer"] ?>" style="margin-left: 15px;">
            </iframe>
        </div>
    </div>

    <?php
    $result = $db->launchQuery("SELECT * FROM cast INNER JOIN `cast-show` ON cast.id = `cast-show`.id_cast WHERE id_show = ?", array($id_shows));
    ?>

    <!-- Cast -->
    <div style="width: 100%;">
        <hr>
        <h1 class="title-border">Cast</h1>
        <div class="row" style="margin: 0.5rem;">
            <?php
            while($cast = $result->fetch_assoc()){
                ?>
                <div class="card card-spacing">
                    <a href="?id=cast&id_cast=<?= $cast["id"] ?>">
                        <img class="card-img-top" src="img/<?= $cast["photo"]?>" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <a href="?id=cast&id_cast=<?= $cast["id"] ?>" class="card-title show-title">
                            <?= $cast["name"] ?>
                        </a>
                    </div>
                </div>
            <?php
            }
            $db->disconnect();?>
        </div>
    </div>

</div>