<?php
$db = new Db();

// Popular
$sql = "SELECT name, photo, id_show, count(id_show) AS count 
        FROM shows INNER JOIN `user-show`
        ON shows.id = `user-show`.id_show
        GROUP BY id_show
        ORDER BY count DESC LIMIT 4";
$result = $db->launchQuery($sql);
?>
<!-- SIDEBAR -->
<aside style="float: right" class="blog-sidebar">
    <div class="sidebar-module">
        <h4>Find shows</h4>
        <ol class="list-unstyled">
            <li><a href="index.php?id=search&search=2017&type=shows&field=year&order=name">2017</a></li>
            <li><a href="index.php?id=search&search=%Drama%&type=shows&field=genre&order=name">Drama</a></li>
            <li><a href="index.php?id=search&search=%Comedy%&type=shows&field=genre&order=name">Comedy</a></li>
            <li><a href="index.php?id=search&search=1&type=shows&field=finished&order=name">Finished</a></li>
            <li><a href="index.php?id=search&search=0&type=shows&field=finished&order=name">Unfinished</a></li>
        </ol>
    </div>
</aside>
<!-- SIDEBAR -->

<div>
    <h3 class="title-border">Popular</h3>
    <div class="row" style="margin-left: 10px;">
        <?php

        while($row = $result->fetch_assoc()){
            ?>
            <div class="card card-spacing">
                <a href="?id=shows&id_shows=<?= $row["id_show"] ?>">
                    <img class="card-img-top" src="img/<?= $row["photo"]?>">
                </a>
                <div class="card-body">
                    <a href="?id=shows&id_shows=<?= $row["id_show"] ?>" class="card-title show-title">
                        <?= $row["name"] ?>
                    </a>
                </div>
            </div>
            <?php
        }

        // Drama Genre
        $sql = "SELECT id, name, photo FROM shows WHERE genre LIKE '%Drama%' LIMIT 4";
        $result = $db->launchQuery($sql);
        ?>
    </div>
    <h3 class="title-border">Drama</h3>
    <div class="row" style="margin-left: 10px;">
        <?php
        while($row = $result->fetch_assoc()){
            ?>
            <div class="card card-spacing">
                <a href="?id=shows&id_shows=<?= $row["id"] ?>">
                    <img class="card-img-top" src="img/<?= $row["photo"]?>">
                </a>
                <div class="card-body">
                    <a href="?id=shows&id_shows=<?= $row["id"] ?>" class="card-title show-title">
                        <?= $row["name"] ?>
                    </a>
                </div>
            </div>
            <?php
        }

        // Action Genre
        $sql = "SELECT id, name, photo FROM shows WHERE genre LIKE '%Action%' LIMIT 4";
        $result = $db->launchQuery($sql);
        ?>
    </div>
    <h3 class="title-border">Action</h3>
    <div class="row"style="margin-left: 10px;">
        <?php

        while($row = $result->fetch_assoc()){
            ?>
            <div class="card card-spacing">
                <a href="?id=shows&id_shows=<?= $row["id"] ?>">
                    <img class="card-img-top" src="img/<?= $row["photo"]?>">
                </a>
                <div class="card-body">
                    <a href="?id=shows&id_shows=<?= $row["id"] ?>" class="card-title show-title">
                        <?= $row["name"] ?>
                    </a>
                </div>
            </div>
            <?php
        }

        // Sci-Fi Genre
        $sql = "SELECT id, name, photo FROM shows WHERE genre LIKE '%Sci-Fi%' LIMIT 4";
        $result = $db->launchQuery($sql);
        ?>
    </div>
    <h3 class="title-border">Sci-Fi</h3>
    <div class="row" style="margin-left: 10px;">
        <?php

        while($row = $result->fetch_assoc()) {
            ?>
            <div class="card card-spacing">
                <a href="?id=shows&id_shows=<?= $row["id"] ?>">
                    <img class="card-img-top" src="img/<?= $row["photo"] ?>">
                </a>
                <div class="card-body">
                    <a href="?id=shows&id_shows=<?= $row["id"] ?>" class="card-title show-title">
                        <?= $row["name"]; ?>
                    </a>
                </div>
            </div>
            <?php
        }
        $db->disconnect();
        ?>
    </div>
</div>
