<?php
if (!isset($_SESSION["user"]))
    header("Location: ../login/login.php");

// Which page to load.
if (isset($_REQUEST["page"]))
    $page = $_REQUEST["page"];
else
    $page = 0;

$db = new Db();
$sql = "SELECT COUNT(*) AS count FROM `user-show` WHERE id_user = " . $_SESSION["id"];
$result = $db->launchQuery($sql);
$row = $result->fetch_assoc();

$count = $row["count"];
$pages = $count / SHOWS_SIZE;
?>
<h1>My Shows</h1>
<hr>
<div class="row">
        <!-- Shows added from the user -->
        <?php
        $db = new Db();
        $sql = "SELECT id, name, photo 
                FROM shows INNER JOIN `user-show` ON shows.id = `user-show`.id_show
                WHERE id_user = ? LIMIT " . $page * SHOWS_SIZE . ", " . SHOWS_SIZE;
        $result = $db->launchQuery($sql, array($_SESSION["id"]));

        while ( $row = $result->fetch_assoc()) {
            ?>
            <div class="card" style="width: 12rem; margin: 0.5rem 0.5rem;">
                <a href="?id=shows&id_shows=<?= $row["id"]?>">
                    <img class="card-img-top" src="img/<?= $row["photo"] ?>" alt="Card image cap">
                </a>
                <div class="card-body">
                    <a href="?id=shows&id_shows=<?= $row["id"]?>" class="card-title" style="font-size: 20px; font-weight: bold; font-family: Helvetica;">
                        <?= $row["name"] ?>
                    </a>
                </div>
            </div>
            <?php
        }
        $db->disconnect();
        ?>
</div>

<nav class="blog-pagination">
    <ul class="pagination">
        <?php
        for ($i = 0; $i < $pages; $i++) { ?>
            <li class="page-item <?php if ($page == $i) echo 'active' ?>">
                <a class="page-link" href="?id=user_shows&page=<?= $i ?>"><?= $i + 1 ?></a>
            </li>
            <?php
        }
        ?>
    </ul>
</nav>
