<?php
// Network site, with all the information
$id_network = $_REQUEST["id_network"];

$db = new Db();
$result = $db->launchQuery("SELECT * FROM network WHERE id = ?", array($id_network));
$show = $result->fetch_assoc();

?>
<div class="row">
    <!-- Show details -->
    <div class="container">
        <div class="card-body" style="width: 70%; float: right;">
            <h1 class="card-title"><?= $show["name"] ?></h1>
            <p class="card-title"><?= $show["country"] . " | Founded: " . $show["founded"] . " | Language: " . $show["language"]?></p>
            <a href="<?= $show["website"] ?>" target="_blank" class="card-text"><?= $show["website"] ?></a>
        </div>
        <img class="card-img-top" src="img/<?= $show["photo"] ?>" alt="Card image cap" style="width: 25%;">
    </div>

    <?php
    $result = $db->launchQuery("SELECT id, name, photo FROM shows INNER JOIN `network-show` ON shows.id = `network-show`.id_show WHERE id_network = ?", array($id_network));
    ?>

    <!-- Shows -->
    <div class="container">
        <hr>
        <h1 class="title-border">Shows</h1>
        <div class="row" style="margin: 0.5rem;">
            <?php
            while($shows = $result->fetch_assoc()){
                ?>
                <div class="card card-spacing">
                    <a href="?id=shows&id_shows=<?= $shows["id"] ?>">
                        <img class="card-img-top" src="img/<?= $shows["photo"]?>">
                    </a>
                    <div class="card-body">
                        <a href="?id=shows&id_shows=<?= $shows["id"] ?>" class="card-title show-title">
                            <?= $shows["name"] ?>
                        </a>
                    </div>
                </div>
                <?php
            }?>
        </div>
    </div>

</div>