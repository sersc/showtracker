-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2017 a las 16:01:55
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `showtracker`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cast`
--

CREATE TABLE `cast` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `biography` varchar(1000) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `photo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cast`
--

INSERT INTO `cast` (`id`, `name`, `biography`, `birthday`, `role`, `photo`) VALUES
(2, 'Travis Fimmel ', 'Travis Fimmel was born near Echuca, Victoria, Australia, to Jennie, a recreation officer for the disabled, and Chris, a cattle farmer. He was raised on a 5500-acre farm located between Melbourne and Sydney. Until the age of seventeen, his life was spent at school and working on the family farm, morning and night-something he continues to relish on his trips back home.                                                ', '1979', 'Actor', 'travis_fimmel.jpg'),
(3, 'Katheryn Winnick', 'Canadian actress Katheryn Winnick stars in the critically acclaimed, Emmy award-winning television series Vikingos (2013). produced by MGM and The History Channel. Entertainment Weekly declared that her role as \"Lagertha\" may be the most exciting feminist character on TV.\"\r\nRecently, Winnick wrapped production on the highly anticipated adaptation of the Stephen King novel La Torre Oscura (2017), starring Academy Award-winner Matthew McConaughey and Golden Globe winner Idris Elba due for release in February 2017. Winnick also stars opposite Gerard Butler in the Warner Brother\'s feature film Geostorm (2017).', '1977', 'Actress', 'katheryn_winnick.jpg'),
(4, 'James Franco', '<p>Known for his breakthrough starring role on Instituto McKinley (1999), James Franco was born in Palo Alto, California on April 19, 1978. His mother is Betsy Franco and his father is Douglas Eugene &quot;Doug&quot; Franco who ran a Silicon Valley business. James&#39;s mother, a writer, has occasionally acted.<br /><br />Growing up with his two younger brothers, Dave Franco, also an actor, and Tom Franco, James graduated from Palo Alto High School in 1996 and went on to attend UCLA, majoring in English. To overcome his shyness, he got into acting while studying there, which, much to his parents&#39; dismay, he left after only one year. After fifteen months of intensive study at Robert Carnegie&#39;s Playhouse West, James began actively pursuing his dream of finding work as an actor in Hollywood.</p>', '1978', 'Actor, Producer', 'james_franco.jpg'),
(5, 'Sarah Gadon', '<p>Sarah Gadon was born in a quiet residential area in Toronto, Ontario, to a teacher mother and a psychologist father. She grew up with the support and encouragement of her parents and older brother, James, and with this was inspired to go headlong into acting and dance alike. Sarah spent much of her adolescence training as a performer as a Junior Associate at the National Ballet School of Canada and as a student at the Claude Watson School for the Performing Arts. She also studied cinema at the prestigious University of Toronto.</p>', '1987', 'Actress, Director', 'sarah_gadon.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cast-show`
--

CREATE TABLE `cast-show` (
  `id_show` int(11) NOT NULL,
  `id_cast` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cast-show`
--

INSERT INTO `cast-show` (`id_show`, `id_cast`) VALUES
(12, 2),
(12, 3),
(13, 4),
(13, 5),
(14, 5),
(15, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `network`
--

CREATE TABLE `network` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `founded` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `language` varchar(50) NOT NULL,
  `photo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `network`
--

INSERT INTO `network` (`id`, `name`, `country`, `founded`, `website`, `language`, `photo`) VALUES
(1, 'History Channel', 'United States', '1995', 'http://www.history.com/', 'English', 'history_channel.jpg'),
(2, 'Showtime', 'United States', '1976', 'http://www.sho.com/', 'English', 'showtime.jpg'),
(3, 'Hulu', 'United States', '2007', 'https://www.hulu.com', 'English', 'hulu.jpg'),
(4, 'Netflix', 'United States', '1997', 'https://www.netflix.com', 'English', 'netflix.jpg'),
(5, 'HBO', 'United States', '1972', 'https://www.hbo.com', 'English', 'hbo.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `network-show`
--

CREATE TABLE `network-show` (
  `id_network` int(11) NOT NULL,
  `id_show` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `network-show`
--

INSERT INTO `network-show` (`id_network`, `id_show`) VALUES
(2, 7),
(3, 8),
(1, 12),
(3, 13),
(4, 14),
(5, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shows`
--

CREATE TABLE `shows` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `seasons` int(11) NOT NULL,
  `synopsis` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `network_id` varchar(50) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `trailer` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `shows`
--

INSERT INTO `shows` (`id`, `name`, `seasons`, `synopsis`, `genre`, `photo`, `year`, `duration`, `network_id`, `finished`, `trailer`) VALUES
(7, 'Shameless', 8, 'An alcoholic man lives in a perpetual stupor while his six children with whom he lives cope as best they can.', 'Comedy, Drama ', 'shameless.jpg', '2011', '46', '2', 0, 'https://www.youtube.com/embed/9tvkYS5cA58'),
(8, 'Future man', 1, 'Josh Futturman, a janitor by day and a gamer by night, is recruited by mysterious visitors to travel through time to prevent the extinction of humanity. ', 'Comedy, Sci-Fi', 'future man.jpg', '2017', '30', '3', 0, 'https://www.youtube.com/embed/vJaUfpbEYIU'),
(12, 'Vikings', 5, '<p>The world of the Vikings is brought to life through the journey of Ragnar Lothbrok, the first Viking to emerge from Norse legend and onto the pages of history - a man on the edge of myth.</p>', 'Action, History', 'vikings.jpg', '2013', '42', '1', 0, 'https://www.youtube.com/embed/Ts_8uk1ddJI'),
(13, '11.22.63', 1, '<p>A high school teacher travels back in time to prevent John F. Kennedy&#39;s assassination.</p>', 'Drama, Mystery, Sci-Fi', '11_22_63.jpg', '2016', '60', '3', 1, 'https://www.youtube.com/embed/NXUx__qQGew'),
(14, 'Alias Grace', 1, '<p>In 19th-century Canada, a psychiatrist weighs whether a murderess should be pardoned due to insanity.</p>', 'Crime, Drama', 'alias_grace.jpg', '2017', '60', '4', 1, 'https://www.youtube.com/embed/A-fofQ9VpPQ'),
(15, 'The Deuce', 1, '<p>A look at life in New York City during the 1970s and &#39;80s when porn and prostitution were rampant in Manhattan.</p>', 'Drama', 'the_deuce.jpg', '2017', '60', '5', 0, 'https://www.youtube.com/embed/Ixd8Tj9a25k');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user-show`
--

CREATE TABLE `user-show` (
  `id_user` int(11) NOT NULL,
  `id_show` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user-show`
--

INSERT INTO `user-show` (`id_user`, `id_show`) VALUES
(3, 13),
(3, 14),
(3, 7),
(3, 15),
(3, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `joined` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `rol`, `joined`) VALUES
(2, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin', '2017-12-11'),
(3, 'sergio', '6ed32edf4e92ab3c0a4dc6f90242953c344051ad', 'Sergio', 'User', '2017-12-11'),
(6, 'nuevo', '5043f762841a8c17c7385efd931b64d46ce0b044', 'nuevo', 'User', '2017-12-15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cast`
--
ALTER TABLE `cast`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cast-show`
--
ALTER TABLE `cast-show`
  ADD KEY `id_cast` (`id_cast`),
  ADD KEY `id_show` (`id_show`);

--
-- Indices de la tabla `network`
--
ALTER TABLE `network`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `network-show`
--
ALTER TABLE `network-show`
  ADD KEY `id_network` (`id_network`),
  ADD KEY `id_show` (`id_show`);

--
-- Indices de la tabla `shows`
--
ALTER TABLE `shows`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user-show`
--
ALTER TABLE `user-show`
  ADD KEY `id_show` (`id_show`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cast`
--
ALTER TABLE `cast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `network`
--
ALTER TABLE `network`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `shows`
--
ALTER TABLE `shows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cast-show`
--
ALTER TABLE `cast-show`
  ADD CONSTRAINT `cast-show_ibfk_2` FOREIGN KEY (`id_show`) REFERENCES `shows` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cast-show_ibfk_3` FOREIGN KEY (`id_cast`) REFERENCES `cast` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `network-show`
--
ALTER TABLE `network-show`
  ADD CONSTRAINT `network-show_ibfk_1` FOREIGN KEY (`id_network`) REFERENCES `network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `network-show_ibfk_2` FOREIGN KEY (`id_show`) REFERENCES `shows` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user-show`
--
ALTER TABLE `user-show`
  ADD CONSTRAINT `user-show_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user-show_ibfk_2` FOREIGN KEY (`id_show`) REFERENCES `shows` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
