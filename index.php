<?php
include("config/config.php");
include("include/funciones.php");

session_start();
if (isset($_SESSION["user"])){
    $user = $_SESSION["user"];
    $rol = $_SESSION["rol"];
    $id = "shows";
}
else
    $user = null;
    $id = "cover";

if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>ShowTracker</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
</head>

<body>

<header>
    <div class="blog-masthead">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">ShowTracker</a>

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <?php

                        if($user != null && $rol == 'admin'){

                            ?>
                            <li class="nav-item">
                                <a class="nav-link" href="admin">
                                    <?php echo "Dashboard"; ?>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php if($user != null) echo "?id=user_shows";?>">
                                <?php if($user != null) echo "My Shows";?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php if($user != null) echo "login/logout.php"; else echo "login/login.php"; ?>">
                                <?php if($user != null) echo "Logout"; else echo "Login";?>
                            </a>
                        </li>
                    </ul>
                    <form action="index.php" method="post" class="form-inline mt-2 mt-md-0">
                        <select name="type" class="custom-select" style="margin-right: 0.5rem;">
                            <option value="shows" selected>Shows</option>
                            <option value="cast">Cast</option>
                            <option value="network">Network</option>
                        </select>
                        <input class="form-control mr-sm-2" name="search" type="text" placeholder="Search" aria-label="Search">
                        <input type="hidden" name="id" value="search"/>
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
</header>

    <main role="main" class="container">
        <?php include($id . ".php") ?>
    </main><!-- /.container -->

    <footer class="blog-footer">
        <p>Sergio Soler</p>
        <p>© 2017 ShowTracker Media Group.</p>
        <p>
            <a href="#">Back to top</a>
        </p>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>

</body>

</html>