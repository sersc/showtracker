<?php
// Cast site, with all the information
$id_cast = $_REQUEST["id_cast"];

$db = new Db();
$result = $db->launchQuery("SELECT * FROM cast WHERE id = ?", array($id_cast));
$show = $result->fetch_assoc();

?>
<div class="row">
    <!-- Show details -->
    <div class="container">
        <div class="card-body" style="width: 70%; float: right;">
            <h1 class="card-title"><?= $show["name"] ?></h1>
            <p class="card-title"><?= $show["role"] . " | Born: " . $show["birthday"]?></p>
            <p class="card-text"><?= $show["biography"] ?></p>
        </div>
        <img class="card-img-top" src="img/<?= $show["photo"] ?>" alt="Card image cap" style="width: 25%;">
    </div>

    <?php
    $result = $db->launchQuery("SELECT id, name, photo FROM shows INNER JOIN `cast-show` ON shows.id = `cast-show`.id_show WHERE id_cast = ?", array($id_cast));
    ?>

    <!-- Known for -->
    <div class="container">
        <hr>
        <h1 class="title-border">Known for</h1>
        <div class="row" style="margin: 0.5rem;">
            <?php
            while($shows = $result->fetch_assoc()){
                ?>
                <div class="card card-spacing">
                    <a href="?id=shows&id_shows=<?= $shows["id"] ?>">
                        <img class="card-img-top" src="img/<?= $shows["photo"]?>" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <a href="?id=shows&id_shows=<?= $shows["id"] ?>" class="card-title show-title">
                            <?= $shows["name"] ?>
                        </a>
                    </div>
                </div>
                <?php
            }?>
        </div>
    </div>

</div>